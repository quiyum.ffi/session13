<?php
class Person{

    public $name="Default Name";
    public $address="Default Address";
    public $phone="Default Phone Number";

    public static $myStaticProperty;
    public static function __callStatic($name, $arguments)
    {
        echo  "I'm inside ".__METHOD__."<br>";
        echo "Wrong static method name = $name<br>";
        echo "<pre>";
        print_r($arguments);
        echo "</pre>";

    }
    public static function doFrequently(){
        echo "I'm doing it frequently<br>";
    }
}
$obj = new person();
Person::$myStaticProperty = "Quiyum";

Person::doFrequently();
echo Person::$myStaticProperty;
echo "<br>";
Person::doFrequentli("wrong static method parameter");

?>